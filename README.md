**Local administrative units of level 1 and 2 - combined**
---------------------------------------
In this repository a shapefile containing administrative boundaries of level 1
and 2 is provided. LAU1 boundaries is particularly used for Denmark since it is
preferred for energy studies in Denmark compared to LAU2. The boudaries of
other regions represent LAU2 level. More information on the shapefile:
about the file is as follows:
- Filetype: shp
- CRS: ETRS89 / ETRS-LAEA
- EPSG: 3035
- Coverage: EU28 + CH + IS + NO 


**Repository Structure**
---------------------
File(s):
- HotmapsLAU.shp

**License**
-------
The license is still to be defined, the **European Climate Assessment & Dataset**
required to apply the ECA&D data policy, therefore for non-commercial research
and non-commercial education projects only.


**Authors**
-------
- Eurostats
- kortforsyningen.dk
- Mostafa Fallahnejad: fallahnejad (at) eeg.tuwien.ac.at


**Acknowledgement**